package exam.coursera.modernartui;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

/**
 * Moder Art UI courera final lab main activity
 */
public class ModernArtActivity extends AppCompatActivity {
    private static final String TAG = "ModernArtActivity ==> ";
    private static final String MOMA_URL = "http://www.moma.org/visit/calendar";

    private SeekBar colorChangeSeekBar;
    private TextView blueTextView; // top left text view
    private TextView greenTextView; // bottom left text view
    private TextView orangeTextView; // top right text view
    private TextView purpleTextView; // bottom right text view

    private RGBColor blueTextViewColor;
    private RGBColor greenTextViewColor;
    private RGBColor orangeTextViewColor;
    private RGBColor purpleTextViewColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modern_art);

        colorChangeSeekBar = (SeekBar) findViewById(R.id.colorSeekBar);
        blueTextView = (TextView) findViewById(R.id.textView);
        greenTextView = (TextView) findViewById(R.id.textView2);
        orangeTextView = (TextView) findViewById(R.id.textView3);
        purpleTextView = (TextView) findViewById(R.id.textView5);

        // storing the original background color values
        blueTextViewColor = getViewRGBColor(blueTextView);
        greenTextViewColor = getViewRGBColor(greenTextView);
        orangeTextViewColor = getViewRGBColor(orangeTextView);
        purpleTextViewColor = getViewRGBColor(purpleTextView);

        colorChangeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                changeBackgroundColor(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                Log.i(TAG, "onStartTrackingTouch");
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Log.i(TAG, "onStopTrackingTouch");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_modern_art, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            final ModernArtDialog modernArtDialog = new ModernArtDialog(ModernArtActivity.this);
            modernArtDialog.initModernArtDialog();
            modernArtDialog.show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Sets the background colors for the view by using the {@link #updateViewColor(RGBColor, int)}
     * method to return the altered color
     *
     * @param progress - the value of the progress bar (varies from 0 to 100)
     */
    private void changeBackgroundColor(final int progress) {
        blueTextView.setBackgroundColor(updateViewColor(blueTextViewColor, progress));
        greenTextView.setBackgroundColor(updateViewColor(greenTextViewColor, progress));
        orangeTextView.setBackgroundColor(updateViewColor(orangeTextViewColor, progress));
        purpleTextView.setBackgroundColor(updateViewColor(purpleTextViewColor, progress));
    }

    /**
     * Reads the background color value from the {@link View} and converts it to a {@link RGBColor}
     * object
     *
     * @param view the view from which we want to get the background color
     * @return an {@link RGBColor}
     */
    private RGBColor getViewRGBColor(final View view) {
        return new RGBColor(((ColorDrawable) view.getBackground()).getColor());
    }

    /**
     * Updates the original {@link RGBColor} color values gathered from the {@link View}
     * by adding to the original values (red, green and blue) the value of the progress (0-100).
     * By using the original {@link RGBColor} values we are making sure that we only increment
     * the values gradually.
     * If the value of one of red, green or blue exceeds the max value {@link RGBColor#MAX_RGB_VALUE}
     * then that max value is returned not to reset color settings
     *
     * @param originalColor the original {@link RGBColor} values gathered from the {@link View}
     * @param updateValue the increment value, the value of the progress bar (from 0 to 100)
     * @return the background color value as an integer, using the {@link RGBColor values}
     */
    private int updateViewColor(final RGBColor originalColor, final int updateValue) {
        final RGBColor updatedRGBColor = new RGBColor(originalColor);
        updatedRGBColor.setRed((originalColor.getRed() + updateValue <= RGBColor.MAX_RGB_VALUE) ? originalColor.getRed() + updateValue : RGBColor.MAX_RGB_VALUE);
        updatedRGBColor.setGreen((originalColor.getGreen() + updateValue <= RGBColor.MAX_RGB_VALUE) ? originalColor.getGreen() + updateValue : RGBColor.MAX_RGB_VALUE);
        updatedRGBColor.setBlue((originalColor.getBlue() + updateValue <= RGBColor.MAX_RGB_VALUE) ? originalColor.getBlue() + updateValue : RGBColor.MAX_RGB_VALUE);

        return Color.rgb(updatedRGBColor.getRed(), updatedRGBColor.getGreen(), updatedRGBColor.getBlue());
    }

    /**
     * The dialog with two buttons, one to visit MoMA website or second to close the dialog
     */
    private class ModernArtDialog extends Dialog {
        public ModernArtDialog(final Context context) {
            super(context);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
        }

        public void initModernArtDialog() {
            setContentView(R.layout.modern_art_dialog);
            final Button dialogViewButton = (Button) findViewById(R.id.viewButton);
            final Button dialogCancelButton = (Button) findViewById(R.id.cancelButton);

            // Registering listener to a button which will close the dialog
            dialogCancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });


            /** Registering a listener for a button which will create an {@link Intent} to view and URL */
            dialogViewButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Intent viewURLIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(MOMA_URL));
                    startActivity(viewURLIntent);
                }
            });
        }
    }
}
