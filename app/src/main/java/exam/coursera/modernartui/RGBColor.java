package exam.coursera.modernartui;


/**
 * Simple class to encapsulate RGB color parameters
 * Not reinventing the wheel here just a helper with custom constructors to best accommodate
 * the needs of the application
 *
 * Created by Zolt Egete on 6/3/15.
 */
public class RGBColor {
    public static final int MAX_RGB_VALUE = 255;

    private int red;
    private int green;
    private int blue;

    public RGBColor(final RGBColor rgbColor) {
        this.red = rgbColor.getRed();
        this.green = rgbColor.getGreen();
        this.blue = rgbColor.getBlue();
    }

    public RGBColor(final int color) {
        this.red = (color >> 16) & 0xFF;
        this.green = (color >> 8) & 0xFF;
        this.blue = (color >> 0) & 0xFF;
    }

    public int getRed() {
        return red;
    }

    public void setRed(int red) {
        this.red = red;
    }

    public int getGreen() {
        return green;
    }

    public void setGreen(int green) {
        this.green = green;
    }

    public int getBlue() {
        return blue;
    }

    public void setBlue(int blue) {
        this.blue = blue;
    }
}
