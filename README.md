# Modern Art UI #

A simple example for the Coursera final Lab

Showing an activity with different colored views, the colors are changing as the seek-bar is moved around.

In the option menu there is a menu option to open a dialog. By selecting the **More information** menu option a dialog opens. 

When the user clicks on the **Visit MOMA** button the browser opens with MoMa website
When the user clicks on the **Not Now** button the dialog will close